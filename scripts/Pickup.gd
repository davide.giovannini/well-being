extends Area2D

export var img_path = "res://img/flower.png"

func _ready():
    $Sprite.texture = load(img_path)


func _on_Pickup_area_entered(area):
    if(area.name == 'Character'):
            area.pickup()  


func _on_Pickup_body_entered(body):
    if(body.name == 'Character'):
            body.jetpack_pickup()
            queue_free()  
