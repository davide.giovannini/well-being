extends Area2D

var _velocity = Vector2(0, 0)
var random;

func _ready():
    random = RandomNumberGenerator.new()
    random.randomize()
    _velocity = Vector2(gen_velocity(), gen_velocity())
    $AnimatedSprite.play("default")
    
func gen_velocity():
    var v = random.randi_range(-20, 30)
    if(v < 0):
        v -= 40
    else:
        v += 40
        
    return v

func _physics_process(delta):
    var speed = 1
    position += _velocity * delta
    
func _on_EnemyFly_body_entered(body):
    print(body)
    if(body.name == 'Walls2'):
        _velocity = -1 * _velocity
    if(body.name == 'Character'):
        body.hurt()
