extends Area2D

export var item_nr = 0
export var img_path = "res://img/flower.png"

func _ready():
    $Sprite.texture = load(img_path)
    pass


func _on_collectable_item_body_entered(body):
    body.pickup()
    queue_free()
