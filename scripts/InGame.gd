extends Node2D


onready var death_screen: ColorRect = $ViewportContainer/ColorRect
onready var death_msg: Label = $ViewportContainer/ColorRect/Label

onready var player = $TestScene/Character

func _ready():
    player.connect("killed", self, "show_death_screen")


func _process(delta):
    if Input.is_action_pressed('ui_select'):
        get_tree().reload_current_scene()


func show_death_screen():
    death_msg.visible = true
    death_msg.modulate = Color(0,0,0,0)

    var tween = get_tree().create_tween()
    tween.tween_property(death_screen, "color", Color(0,0,0,1),2).set_trans(Tween.TRANS_EXPO).set_ease(Tween.EASE_IN)
    tween.tween_property(death_msg, "modulate", Color.white,2).set_trans(Tween.TRANS_EXPO).set_ease(Tween.EASE_OUT)
    tween.tween_interval(2)
    tween.tween_property(death_msg, "modulate", Color.black,2).set_trans(Tween.TRANS_EXPO).set_ease(Tween.EASE_OUT)
    tween.tween_callback(self, "go_back_to_menu")




func go_back_to_menu():
   get_tree().change_scene("res://main.tscn")
