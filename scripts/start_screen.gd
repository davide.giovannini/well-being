extends Control


onready var button: Button = $Button



func _ready():
    button.grab_focus()


func _on_Button_pressed():
   get_tree().change_scene("res://InGame.tscn")


func _on_Button2_pressed():
    get_tree().change_scene("res://3d/Fall.tscn")
