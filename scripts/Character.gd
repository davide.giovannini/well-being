extends KinematicBody2D

signal killed

const DRAG = 0.95
const DRAG_WHILE_MOVING = 0.997
const ACC = 15

var max_speed = 250
var speed = 0
var velocity = Vector2()

var pickups_found = 0


onready var audio_stream_player : AudioStreamPlayer2D = $AudioStreamPlayer2D
onready var particles: CPUParticles2D = $CPUParticles2D
onready var sprite: Sprite = $Sprite

var streams = {
    "hurt": load("res://sound/Explosion4.wav"),
    "powerup": load("res://sound/Powerup3.wav")
   }

func get_input():
    # Detect up/down/left/right keystate and only move when pressed
    var drag = DRAG
    if Input.is_action_pressed('move_right'):
        speed += ACC
        speed = min(speed, max_speed)
        if speed > 0: # already going right
            drag = DRAG_WHILE_MOVING
    if Input.is_action_pressed('move_left'):
        speed -= ACC
        speed = max(speed, -max_speed)
        if speed < 0: # already going left
            drag = DRAG_WHILE_MOVING


    speed = speed * drag
    velocity.x = speed

func _physics_process(delta):
    get_input()
    var collision_info = move_and_collide(velocity * delta)
    if collision_info:
        speed = -speed

func hurt():
    audio_stream_player.stream = streams["hurt"]
    audio_stream_player.play()
    print("HURT")


func pickup():
    audio_stream_player.stream = streams["powerup"]
    audio_stream_player.play()
    print("PICKUP")
    
func jetpack_pickup():
    pickups_found += 1 

func kill():
    sprite.visible = false
    particles.emitting = true
    emit_signal("killed")
    get_tree().create_timer(1)
